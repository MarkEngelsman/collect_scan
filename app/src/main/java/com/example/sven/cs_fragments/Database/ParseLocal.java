package com.example.sven.cs_fragments.Database;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Valentijn on 11-1-2016.
 */
public class ParseLocal {

    public static final String DebugParseLocal = "ParseLocal";
    public List<ParseObject> array = new ArrayList<>();

    public ParseLocal()
    {
        testPutInBackground();
    }

    //insert an entire list into the Parse local storage.
    public boolean insertListIntoLocal(List<ParseObject> list)
    {
        try
        {
            ParseObject.pinAllInBackground(list);
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }

    //insert an object into the Parse local storage.
    public boolean insertObjectIntoLocal(ParseObject parseObject)
    {
        try
        {
            parseObject.pinInBackground();
            Log.d(DebugParseLocal, "Inserted: " + parseObject.get("Username"));
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }

    //remove list of objects from the local storage.
    public boolean deleteListFromLocal(List<ParseObject> list)
    {
        try
        {
            ParseObject.unpinAllInBackground(list);
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }

    //delete specific object from the local storage.
    public boolean deleteObjectFromLocal(ParseObject object)
    {
        try
        {
            object.unpinInBackground();
            Log.d(DebugParseLocal, "Removed: " + object.get("Username"));
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }

    //get an entire table from the Parse local storage.
    public void getTableFromLocalStorage(String tableName)
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery(tableName);
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                array = list;
            }
        });
    }

    public void testPutInBackground() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User");

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null)
                {
                    for(int i = 0; i < list.size(); i++)
                    {
                         ParseObject obj = list.get(i);
                         Log.d(DebugParseLocal, "Cloud: " + obj.get("Username"));
                         obj.pinInBackground();
                    }

                }else
                {
                    Log.d(DebugParseLocal,"Error @ 21");
                }
            }
        });
    }

    public void testGetFromBackground()
    {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("User");

        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null)
                {
                    for(int i = 0; i < list.size(); i++)
                    {
                        ParseObject obj = list.get(i);
                        Log.d(DebugParseLocal, "Local: " + obj.get("Username"));
                        obj.unpinInBackground();
                    }

                }else
                {
                    Log.d(DebugParseLocal,"Error @ 44");
                }
            }
        });
    }
}
