package com.example.sven.cs_fragments.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.sven.cs_fragments.Fragments.HomeFragment_1;
import com.example.sven.cs_fragments.Fragments.HomeFragment_2;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 04/01/2016.
 */
public class HomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        HomeFragment_1 frag1 = new HomeFragment_1();
        HomeFragment_2 frag2 = new HomeFragment_2();

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.home_layout, frag1, "HomeBarFragment");
        FragmentTransaction transaction2 = manager.beginTransaction();
        transaction2.add(R.id.home_layout, frag2, "HomeButtonsFragment");
        transaction.commit();


    }
}
