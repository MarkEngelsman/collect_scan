package com.example.sven.cs_fragments.Database;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by sha on 2016-01-11.
 */
public class ImageController {

    public static final String userOne = "User_One";
    public static final String userTwo = "User_Two";
    List<ParseObject> friendsList;

    public void addImage(String mUserOne, String mUserTwo) {
        ParseObject parseObjectGame = new ParseObject("Friendship");
        parseObjectGame.put(userOne, mUserOne);
        parseObjectGame.put(userTwo, mUserTwo);
        parseObjectGame.saveInBackground();

    }
}
/*
    public List<ParseObject> getImage(String barcode) {

        ParseQuery<ParseObject> query = ParseQuery.getQuery("Image");

        query.whereEqualTo(Barcode, barcode);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> items, ParseException e) {
                if (e == null) {

                    for (ParseObject i : items) {
                        images.add(i);
                    }

                } else {
                    Log.d("item", "Error: " + e.getMessage());
                }
            }
        });


        return images;
    }
}*/
