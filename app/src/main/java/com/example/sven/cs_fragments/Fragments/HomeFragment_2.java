package com.example.sven.cs_fragments.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.sven.cs_fragments.Activities.CollectionActivity;
import com.example.sven.cs_fragments.Activities.FriendsActivity;
import com.example.sven.cs_fragments.Activities.ProfileActivity;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 04/01/2016.
 */
public class HomeFragment_2 extends Fragment {
    Button profile;
    Button friends;
    Button collection;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_fragment_2, container, false);

        profile = (Button) v.findViewById(R.id.button_h_profile);
        profile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(intent);

            }
        });

        friends = (Button) v.findViewById(R.id.button_h_friends);
        friends.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent intent = new Intent(getActivity(), FriendsActivity.class);
                startActivity(intent);

            }
        });

        collection = (Button) v.findViewById(R.id.button_h_mycollection);
        collection.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent intent = new Intent(getActivity(), CollectionActivity.class);
                startActivity(intent);

            }
        });

       // v.setBackground(getActivity().getResources().getDrawable(R.drawable.wallpaperapp));


        return v;
    }
}