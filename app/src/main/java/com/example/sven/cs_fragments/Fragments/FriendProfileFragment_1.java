package com.example.sven.cs_fragments.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.sven.cs_fragments.Activities.FriendsActivity;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 6-1-2016.
 */
public class FriendProfileFragment_1 extends Fragment {

    Button back;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.friendprofile_fragment_1, container, false);

        back = (Button) v.findViewById(R.id.button_fp_back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent intent = new Intent(getActivity(), FriendsActivity.class);
                startActivity(intent);

            }
        });

        return v;
    }
}
