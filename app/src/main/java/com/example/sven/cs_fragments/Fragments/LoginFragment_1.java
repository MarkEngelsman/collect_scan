package com.example.sven.cs_fragments.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sven.cs_fragments.Activities.HomeActivity;
import com.example.sven.cs_fragments.Activities.SignupActivity;
import com.example.sven.cs_fragments.R;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.text.ParseException;


/**
 * Created by Sven on 04/01/2016.
 */
public class LoginFragment_1 extends Fragment{
    Button signup;
    Button login;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_fragment_1, container, false);

        signup = (Button) v.findViewById(R.id.button_signup);
        signup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent intent = new Intent(getActivity(), SignupActivity.class);
                startActivity(intent);

            }
        });

        login = (Button) v.findViewById(R.id.button_login);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                checkConnection(getActivity().getApplicationContext()
                );

            }
        });

        return v;

    }

    //Login via Parse.com cloud.
    public void login()
    {
        //get user input
        EditText un = (EditText)getView().findViewById(R.id.editText_username);
        EditText ps = (EditText)getView().findViewById(R.id.editText_password);

        try
        {
            //Make sure logged in users is empty.
            ParseObject.unpinAllInBackground(getResources().getString(R.string.LoggedInUser));

            //get user information from Parse.com where it matches the user input.
            ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
            query.whereEqualTo("Username", un.getText().toString());
            query.whereEqualTo("Password", ps.getText().toString());
            ParseObject user = query.getFirst();

            //Use the same query for the local datastore
            query.fromLocalDatastore();

            //if there is no device user yet, save device user.
            if(query.count() == 0)
            {
                user.pinInBackground("User");
            }

            //save the logged in user in the local datastore.
            user.pinInBackground(getResources().getString(R.string.LoggedInUser));

            //if there was no error on query.getFirst() start next activity.
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            startActivity(intent);

        } catch(com.parse.ParseException d)
        {
            if(d.getCode() == com.parse.ParseException.OBJECT_NOT_FOUND)
            {
                //if the user input was not correct and caused the above error, show message.
                showMessage("" + getResources().getString(R.string.User_Password_Incorrect));
                ParseObject.unpinAllInBackground(getResources().getString(R.string.LoggedInUser));
            }
        }
        catch(Exception e)
        {
            //if another error occurred show message.
            showMessage("" + getResources().getString(R.string.User_Password_Incorrect) + e.toString());
            ParseObject.unpinAllInBackground(getResources().getString(R.string.LoggedInUser));
        }
    }

    //Login via local storage (if user has no internet connection).
    public void loginOffline()
    {
        try
        {
            //Make sure logged in users is empty.
            ParseObject.unpinAllInBackground(getResources().getString(R.string.LoggedInUser));

            //get user input
            EditText un = (EditText)getView().findViewById(R.id.editText_username);
            EditText ps = (EditText)getView().findViewById(R.id.editText_password);

            //get user information from local storage where it matches the user input.
            ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
            query.whereEqualTo("Username", un.getText().toString());
            query.whereEqualTo("Password", ps.getText().toString());
            query.fromLocalDatastore();
            ParseObject user = query.getFirst();

            //save the logged in user in the local datastore.
            user.pinInBackground(getResources().getString(R.string.LoggedInUser));

            //if there was no error on query.getFirst() start next activity.
            showMessage((getResources().getString(R.string.LoggedInOffline)));
            Intent intent = new Intent(getActivity(), HomeActivity.class);
            startActivity(intent);


        }catch(com.parse.ParseException d)
        {
            //if the user input was not correct and caused the above error, show message.
            showMessage("" + getResources().getString(R.string.User_Password_Incorrect));
            ParseObject.unpinAllInBackground(getResources().getString(R.string.LoggedInUser));
        }catch(Exception e)
        {
            //if another error occurred show message.
            showMessage("" + getResources().getString(R.string.User_Password_Incorrect) + e.toString());
            ParseObject.unpinAllInBackground(getResources().getString(R.string.LoggedInUser));
        }
    }

    //Shows a toast with a custom message.
    private void showMessage(String text)
    {
        final String finalText = text;
        Toast.makeText(getActivity(), finalText, Toast.LENGTH_SHORT).show();
    }

    //modified from: http://stackoverflow.com/questions/28376264/how-to-know-if-there-is-internet-when-using-parse-com-android
    private void checkConnection(Context context)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if(networkInfo != null)
        {
            login();
        }else
        {
            loginOffline();
        }
    }
}
