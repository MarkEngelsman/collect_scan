package com.example.sven.cs_fragments.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 6-1-2016.
 */
public class FriendsFragment_2 extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.friends_fragment_2, container, false);
    }
}
