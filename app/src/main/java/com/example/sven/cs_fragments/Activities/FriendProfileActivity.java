package com.example.sven.cs_fragments.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.sven.cs_fragments.Fragments.FriendProfileFragment_1;
import com.example.sven.cs_fragments.Fragments.FriendProfileFragment_2;
import com.example.sven.cs_fragments.Fragments.FriendsFragment_1;
import com.example.sven.cs_fragments.Fragments.FriendsFragment_2;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 6-1-2016.
 */
public class FriendProfileActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friendprofile);

        FriendProfileFragment_1 frag1 = new FriendProfileFragment_1();
        FriendProfileFragment_2 frag2 = new FriendProfileFragment_2();

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.friendprofile_layout, frag1, "FriendprofileBarFragment");
        FragmentTransaction transaction2 = manager.beginTransaction();
        transaction2.add(R.id.friendprofile_layout, frag2, "ProfileFragment");
        transaction.commit();
    }
}
