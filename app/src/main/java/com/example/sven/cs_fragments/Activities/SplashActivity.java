package com.example.sven.cs_fragments.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.sven.cs_fragments.Fragments.SplashFragment_1;
import com.example.sven.cs_fragments.R;
import com.parse.Parse;

/**
 * Created by Sven on 11-1-2016.
 */
public class SplashActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        SplashFragment_1 frag = new SplashFragment_1();
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.splash_layout, frag, "SplashFragment");
        transaction.commit();

        Parse.enableLocalDatastore(this);

        try{

            Parse.initialize(this);

        }catch (Exception e)
        {

        }


    }
}
