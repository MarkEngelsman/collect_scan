package com.example.sven.cs_fragments.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.sven.cs_fragments.Fragments.BarFragment_1;
import com.example.sven.cs_fragments.Fragments.ProfileFragment_1;
import com.example.sven.cs_fragments.Fragments.ProfileFragment_2;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 5-1-2016.
 */
public class ProfileActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //BarFragment_1 frag1 = new BarFragment_1();
        ProfileFragment_1 frag2 = new ProfileFragment_1();
        ProfileFragment_2 frag3 = new ProfileFragment_2();

        FragmentManager manager = getFragmentManager();

        //FragmentTransaction transaction = manager.beginTransaction();
        //transaction.add(R.id.profile_layout, frag1, "BarFragment");

        FragmentTransaction transaction2 = manager.beginTransaction();
        transaction2.add(R.id.profile_layout, frag2, "ProfiletopFragment");

        FragmentTransaction transaction3 = manager.beginTransaction();
        transaction3.add(R.id.profile_layout, frag3, "ProfilemainFragment");

        transaction2.commit();
    }

}
