package com.example.sven.cs_fragments.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.sven.cs_fragments.Fragments.Collection_Fragment_1;
import com.example.sven.cs_fragments.Fragments.Collection_Fragment_2;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 6-1-2016.
 */
public class CollectionActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);

        Collection_Fragment_1 frag1 = new Collection_Fragment_1();
        Collection_Fragment_2 frag2 = new Collection_Fragment_2();

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.collection_layout, frag1, "CollectionBarFragment");
        FragmentTransaction transaction2 = manager.beginTransaction();
        transaction2.add(R.id.collection_layout, frag2, "CollectionlistFragment");
        transaction.commit();
    }
}
