package com.example.sven.cs_fragments.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.sven.cs_fragments.Activities.FriendProfileActivity;
import com.example.sven.cs_fragments.Activities.HomeActivity;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 6-1-2016.
 */
public class FriendsFragment_1 extends Fragment {

    Button back;
    Button menu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.friends_fragment_1, container, false);

        back = (Button) v.findViewById(R.id.button_f_back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);

            }
        });
        //test voor friendprofile deze moet zodra je een naam in de lijst aanklikt
        menu = (Button) v.findViewById(R.id.button_f_menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent intent = new Intent(getActivity(), FriendProfileActivity.class);
                startActivity(intent);

            }
        });

        return v;
    }
}
