package com.example.sven.cs_fragments.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.sven.cs_fragments.Fragments.LoginFragment_1;
import com.example.sven.cs_fragments.R;


public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginFragment_1 frag = new LoginFragment_1();
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.login_layout, frag, "LoginFragment");
        transaction.commit();
    }
}

