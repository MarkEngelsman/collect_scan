package com.example.sven.cs_fragments.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.sven.cs_fragments.Activities.LoginActivity;
import com.example.sven.cs_fragments.R;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Sven on 5-1-2016.
 */
public class ProfileFragment_2 extends Fragment {

    Button signout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_fragment_2, container, false);

        signout = (Button) v.findViewById(R.id.button_p_signout);
        signout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {

                //remove the logged in user from the local data store.
                ParseObject.unpinAllInBackground("LoggedInUser");

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);

            }
        });

        return v;
    }


}
