package com.example.sven.cs_fragments.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Valentijn on 4-1-2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    //name of the database.
    public static final String DATABASE_NAME = "application_local_database";

    //Table and column names for the collection_item table.
    public static final String COLLECTIONITEM_TABLE_NAME = "collection_item";
    public static final String COLLECTIONITEM_COLUMN_ID = "id";
    public static final String COLLECTIONITEM_COLUMN_USERNAME = "username";
    public static final String COLLECTIONITEM_COLUMN_TITLE = "title";
    public static final String COLLECTIONITEM_COLUMN_PLATFORM = "platform";
    public static final String COLLECTIONITEM_COLUMN_REGION = "region";
    public static final String COLLECTIONITEM_COLUMN_BARCODE = "barcode";
    public static final String COLLECTIONITEM_COLUMN_C = "c";
    public static final String COLLECTIONITEM_COLUMN_I = "i";
    public static final String COLLECTIONITEM_COLUMN_B = "b";
    public static final String COLLECTIONITEM_COLUMN_CONDITION = "condition";

    //Table and column names for the user table.
    public static final String USER_TABLE_NAME = "user";
    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_USERNAME = "username";
    public static final String USER_COLUMN_EMAIL = "email";
    public static final String USER_COLUMN_PASSWORD = "password";
    public static final String USER_COLUMN_ROL = "rol";
    public static final String USER_COLUMN_DATEOFBIRTH = "date_of_birth";
    public static final String USER_COLUMN_ABOUTME = "about_me";
    public static final String USER_COLUMN_AVATAR = "avatar";
    public static final String USER_COLUMN_COUNTRY = "country";
    public static final String USER_COLUMN_PRIVACYSETTING = "privacy_setting";

    //debug tag
    private static final String DB_DEBUG_TAG = "SQLITE DB";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.d(DB_DEBUG_TAG, "Creating tables..");

        //create user table
        db.execSQL("create table " + USER_TABLE_NAME +
                        " (" + USER_COLUMN_ID + " integer primary key autoincrement not null," + USER_COLUMN_USERNAME + " text not null, " + USER_COLUMN_EMAIL + " text not null, " +
                        USER_COLUMN_PASSWORD + " text not null, " +USER_COLUMN_ROL + " text not null, " +  USER_COLUMN_DATEOFBIRTH + " text not null, " + USER_COLUMN_ABOUTME + " text, "
                        + USER_COLUMN_AVATAR + " text, " + USER_COLUMN_COUNTRY + " text, " + USER_COLUMN_PRIVACYSETTING + " text)"
        );

        //create collection_item table
        db.execSQL("create table " + COLLECTIONITEM_TABLE_NAME +
                        " (" + COLLECTIONITEM_COLUMN_ID + " integer primary key," + COLLECTIONITEM_COLUMN_USERNAME + " text, " + COLLECTIONITEM_COLUMN_TITLE + " text, " +
                        COLLECTIONITEM_COLUMN_PLATFORM + " text, " + COLLECTIONITEM_COLUMN_REGION + " text, " +  COLLECTIONITEM_COLUMN_BARCODE + " integer, " + COLLECTIONITEM_COLUMN_C + " integer, "
                        + COLLECTIONITEM_COLUMN_I + " integer, " + COLLECTIONITEM_COLUMN_B + " integer, " + COLLECTIONITEM_COLUMN_CONDITION + " text, " +
                        "foreign key(" + COLLECTIONITEM_COLUMN_USERNAME + ") references " + USER_TABLE_NAME + "(" + USER_COLUMN_USERNAME + "))"
        );

        Log.d(DB_DEBUG_TAG, "Tables created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + COLLECTIONITEM_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE_NAME);
        onCreate(db);
    }

    // insert a new user into the local database.
    public boolean insertUser(String username, String email, String password, String rol, String dateOfBirth, String aboutMe, String avatar, String country, String privacySetting)
    {
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues v = new ContentValues();
            v.put(USER_COLUMN_USERNAME, username);
            v.put(USER_COLUMN_EMAIL, email);
            v.put(USER_COLUMN_PASSWORD, password);
            v.put(USER_COLUMN_ROL, rol);
            v.put(USER_COLUMN_DATEOFBIRTH, dateOfBirth);
            v.put(USER_COLUMN_ABOUTME, aboutMe);
            v.put(USER_COLUMN_AVATAR, avatar);
            v.put(USER_COLUMN_COUNTRY, country);
            v.put(USER_COLUMN_PRIVACYSETTING, privacySetting);
            db.insert(USER_TABLE_NAME, null, v);
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }

    //update the user
    public boolean updateUser(int id, String username, String email, String password, String rol, String aboutMe, String avatar, String country, String privacySetting)
    {
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues v = new ContentValues();
            v.put(USER_COLUMN_USERNAME, username);
            v.put(USER_COLUMN_EMAIL, email);
            v.put(USER_COLUMN_PASSWORD, password);
            v.put(USER_COLUMN_ROL, rol);
            v.put(USER_COLUMN_ABOUTME, aboutMe);
            v.put(USER_COLUMN_AVATAR, avatar);
            v.put(USER_COLUMN_COUNTRY, country);
            v.put(USER_COLUMN_PRIVACYSETTING, privacySetting);
            db.update(USER_TABLE_NAME, v, USER_COLUMN_ID + " = ?", new String[]{Integer.toString(id)});
            return true;

        }catch(Exception e)
        {
            return false;
        }
    }

    //get a specific user
    public Cursor getUser(int id)
    {
        try
        {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c =  db.rawQuery( "select * from " + USER_TABLE_NAME + " where " + USER_COLUMN_ID + "="+id+"", null );
            return c;
        }catch(Exception e)
        {
            return null;
        }

    }

    //get all users.
    public Cursor getAllUsers()
    {
        try{
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery("select * from " + USER_TABLE_NAME, null);
            return c;

        }catch(Exception e)
        {
            return null;
        }
    }

    //delete specific user
    public boolean deleteUser(int id)
    {
        try
        {
            SQLiteDatabase db = this.getReadableDatabase();
            db.delete(USER_TABLE_NAME, USER_COLUMN_ID + " = ?", new String[]{Integer.toString(id)});
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }

    //insert an item
    public boolean insertItem(String username, String title, String platform, String region, int barcode, int c, int i, int b, String condition)
    {
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues v = new ContentValues();
            v.put(COLLECTIONITEM_COLUMN_USERNAME, username);
            v.put(COLLECTIONITEM_COLUMN_TITLE, title);
            v.put(COLLECTIONITEM_COLUMN_PLATFORM, platform);
            v.put(COLLECTIONITEM_COLUMN_REGION, region);
            v.put(COLLECTIONITEM_COLUMN_BARCODE, barcode);
            v.put(COLLECTIONITEM_COLUMN_C, c);
            v.put(COLLECTIONITEM_COLUMN_I, i);
            v.put(COLLECTIONITEM_COLUMN_B, b);
            v.put(COLLECTIONITEM_COLUMN_CONDITION, condition);
            db.insert(COLLECTIONITEM_TABLE_NAME, null, v);
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }

    //update specific item
    public boolean updateItem(int id, String title, String platform, String region, int barcode, int c, int i, int b, String condition)
    {
        try{
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues v = new ContentValues();
            v.put(COLLECTIONITEM_COLUMN_TITLE, title);
            v.put(COLLECTIONITEM_COLUMN_PLATFORM, platform);
            v.put(COLLECTIONITEM_COLUMN_REGION, region);
            v.put(COLLECTIONITEM_COLUMN_BARCODE, barcode);
            v.put(COLLECTIONITEM_COLUMN_C, c);
            v.put(COLLECTIONITEM_COLUMN_I, i);
            v.put(COLLECTIONITEM_COLUMN_B, b);
            v.put(COLLECTIONITEM_COLUMN_CONDITION, condition);
            db.update(COLLECTIONITEM_TABLE_NAME, v, COLLECTIONITEM_COLUMN_ID + " = ?", new String[]{Integer.toString(id)});
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }

    //get specific item
    public Cursor getItem(int id)
    {
        try
        {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c =  db.rawQuery( "select * from " + COLLECTIONITEM_TABLE_NAME + " where " + COLLECTIONITEM_COLUMN_ID + "="+id+"", null );
            return c;
        }catch(Exception e)
        {
            return null;
        }
    }

    //get all items.
    public Cursor getAllItems()
    {
        try{
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery("select * from " + COLLECTIONITEM_TABLE_NAME, null);
            return c;

        }catch(Exception e)
        {
            return null;
        }
    }

    //delete specific item
    public boolean deleteItem(int id)
    {
        try
        {
            SQLiteDatabase db = this.getReadableDatabase();
            db.delete(COLLECTIONITEM_TABLE_NAME, COLLECTIONITEM_COLUMN_ID + " = ?", new String[]{Integer.toString(id)});
            return true;
        }catch(Exception e)
        {
            return false;
        }
    }
}
