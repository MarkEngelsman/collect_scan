package com.example.sven.cs_fragments.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.example.sven.cs_fragments.Fragments.FriendsFragment_1;
import com.example.sven.cs_fragments.Fragments.FriendsFragment_2;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 6-1-2016.
 */
public class FriendsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        FriendsFragment_1 frag1 = new FriendsFragment_1();
        FriendsFragment_2 frag2 = new FriendsFragment_2();

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.friends_layout, frag1, "FriendsBarFragment");
        FragmentTransaction transaction2 = manager.beginTransaction();
        transaction2.add(R.id.friends_layout, frag2, "FriendslistFragment");
        transaction.commit();


    }
}
