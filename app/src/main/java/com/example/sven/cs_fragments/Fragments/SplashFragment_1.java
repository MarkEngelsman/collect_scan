package com.example.sven.cs_fragments.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.example.sven.cs_fragments.Activities.LoginActivity;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 11-1-2016.
 */
public class SplashFragment_1 extends Fragment {

    ImageView iv;
    Animation an;
    Animation an2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.splash_fragment_1, container, false);


        iv = (ImageView) v.findViewById(R.id.imageView3);
        an = AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.rotate);
        an2 = AnimationUtils.loadAnimation(getActivity().getBaseContext(), R.anim.abc_fade_out);

        iv.startAnimation(an);
        an.setAnimationListener(new Animation.AnimationListener() {

            @Override
        public void onAnimationStart(Animation animation){

            }

            @Override
        public void onAnimationEnd(Animation animation){
                iv.startAnimation(an2);
                getActivity().finish();
                Intent i = new Intent(getActivity().getBaseContext(), LoginActivity.class);
                startActivity(i);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        return v;
    }
}
