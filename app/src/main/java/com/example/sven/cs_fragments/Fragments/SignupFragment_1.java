package com.example.sven.cs_fragments.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sven.cs_fragments.Activities.LoginActivity;
import com.example.sven.cs_fragments.Database.ParseLocal;
import com.example.sven.cs_fragments.R;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sven on 04/01/2016.
 */
public class SignupFragment_1 extends Fragment{
    Button back;
    Button signup;

    public static final String Username = "Username";
    public static final String Password = "Password";
    public static final String Email    = "Email";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.signup_fragment_1, container, false);

        back = (Button) v.findViewById(R.id.button_s_back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);

            }
        });

        signup = (Button) v.findViewById(R.id.button_s_signup);
        signup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {


                    InsertUser();

            }
        });
        return v;
    }

    //Shows a toast with a custom message.
    private void showMessage(String text)
    {
        final String finalText = text;
        //runOnUiThread called because called in different thread.
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getActivity(), finalText, Toast.LENGTH_SHORT).show();
            }
        });
    }

    //Check if the information from the input fields can be inserted into the database. if so, add a new user.
    public void InsertUser()
    {
        EditText un = (EditText)getView().findViewById(R.id.editText_s_username);
        EditText ps = (EditText)getView().findViewById(R.id.editText_s_password);
        EditText em = (EditText)getView().findViewById(R.id.editText_s_email);

        //get user input.
        final String username = un.getText().toString();
        final String password = ps.getText().toString();
        final String email = em.getText().toString();

        //create a list of queries.
        List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();

        //create two queries to check if username or email already exist in the Parse.com database.
        ParseQuery<ParseObject> queryUser = ParseQuery.getQuery("User");
        queryUser.whereEqualTo(Username, username);
        ParseQuery<ParseObject> queryEmail = ParseQuery.getQuery("User");
        queryEmail.whereEqualTo(Email, email);

        //add the two queries to the query list.
        queries.add(queryUser);
        queries.add(queryEmail);

        //create one query from the two queries.
        ParseQuery<ParseObject> superQuery = ParseQuery.or(queries);

        //execute the query and do something with the results.
        superQuery.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, com.parse.ParseException e) {
                try
                {
                    if(e == null)
                    {
                        //the given username and/or email already exists within the database
                        showMessage(getResources().getString(R.string.Username_Email));
                    }
                    else
                    {
                        //The object was not found, the user can be inserted.
                        if(e.getCode() == com.parse.ParseException.OBJECT_NOT_FOUND)
                        {
                            ParseObject parseObjectUser = new ParseObject("User");
                            parseObjectUser.put(Username, username);
                            parseObjectUser.put(Password, password);
                            parseObjectUser.put(Email, email);
                            parseObjectUser.saveInBackground();

                            showMessage("" + getResources().getString(R.string.SignedUp));

                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                        }
                        else
                        {
                            //an error occurred getting the records.
                            showMessage("" + getResources().getString(R.string.Error_Occurred) +  e.toString());
                        }
                    }
                } catch (Exception d) {
                    showMessage("" + getResources().getString(R.string.Error_Occurred) + d.toString());
                }
            }
        });
    }
}
