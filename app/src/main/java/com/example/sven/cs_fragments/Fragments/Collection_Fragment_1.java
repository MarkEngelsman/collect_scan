package com.example.sven.cs_fragments.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.sven.cs_fragments.Activities.HomeActivity;
import com.example.sven.cs_fragments.R;

/**
 * Created by Sven on 6-1-2016.
 */
public class Collection_Fragment_1 extends Fragment {

    Button back;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.collection_fragment_1, container, false);

        back = (Button) v.findViewById(R.id.button_c_back);
        back.setOnClickListener(new View.OnClickListener() {
            public void onClick(View V) {
                Intent intent = new Intent(getActivity(), HomeActivity.class);
                startActivity(intent);

            }
        });

        return v;
    }
}
